﻿namespace WebAPI_Ef_OpenAPI.Models.Entities
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public int? ProfessorId { get; set; }

        public virtual Professor? Professor { get; set; }
        public virtual Project? Project { get; set; }

        public virtual ICollection<Subject> Subjects { get; set; } = new HashSet<Subject>();
    }
}
