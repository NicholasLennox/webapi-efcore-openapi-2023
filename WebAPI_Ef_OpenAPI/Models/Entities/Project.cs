﻿namespace WebAPI_Ef_OpenAPI.Models.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public int StudentId { get; set; }

        public virtual Student Student { get; set; } = null!;
    }
}
