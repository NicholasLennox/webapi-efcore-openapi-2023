﻿namespace WebAPI_Ef_OpenAPI.Models.Entities
{
    public class Subject
    {
        public int Id { get; set; }
        public string SubCode { get; set; } = null!;
        public string SubTitle { get; set; } = null!;
        public int? ProfessorId { get; set; }

        public virtual Professor? Professor { get; set; }
        public virtual ICollection<Student> Students { get; set; } = new HashSet<Student>();
    }
}
