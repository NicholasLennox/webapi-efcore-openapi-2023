﻿namespace WebAPI_Ef_OpenAPI.Models.Entities
{
    public class Professor
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Field { get; set; }

        public virtual ICollection<Student> Students { get; set; } = new HashSet<Student>();
        public virtual ICollection<Subject> Subjects { get; set; } = new HashSet<Subject>();
    }
}
