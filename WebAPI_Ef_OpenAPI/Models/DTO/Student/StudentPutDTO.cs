﻿namespace WebAPI_Ef_OpenAPI.Models.DTO.Student
{
    public class StudentPutDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
