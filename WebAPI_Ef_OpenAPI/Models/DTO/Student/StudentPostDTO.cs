﻿namespace WebAPI_Ef_OpenAPI.Models.DTO.Student
{
    public class StudentPostDTO
    {
        public string Name { get; set; }
        public int? Professor { get; set; }
    }
}
