﻿namespace WebAPI_Ef_OpenAPI.Models.DTO.Student
{
    public class StudentDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? Professor { get; set; }
        public int? Project { get; set; }
        public int[] Subjects { get; set; }    
    }
}
