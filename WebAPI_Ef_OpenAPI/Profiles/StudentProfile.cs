﻿using AutoMapper;
using WebAPI_Ef_OpenAPI.Models.DTO.Student;
using WebAPI_Ef_OpenAPI.Models.Entities;

namespace WebAPI_Ef_OpenAPI.Profiles
{
    public class StudentProfile : Profile
    {
        public StudentProfile() 
        {
            CreateMap<Student, StudentDTO>()
                .ForMember(dto => dto.Professor, opt => opt.MapFrom(s => s.ProfessorId))
                .ForMember(dto => dto.Project, opt => opt.MapFrom(s => s.Project.Id))
                .ForMember(dto => dto.Subjects, opt => opt.MapFrom(s => s.Subjects.Select(s => s.Id).ToArray()));
        }
    }
}
