﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using WebAPI_Ef_OpenAPI.Models.DTO.Student;
using WebAPI_Ef_OpenAPI.Models.Entities;
using WebAPI_Ef_OpenAPI.Services.Students;

namespace WebAPI_Ef_OpenAPI.Controllers
{
    [ApiController]
    [Route("api/v1/students")]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentService _studentService;
        private readonly IMapper _mapper;

        public StudentsController(IStudentService studentService, IMapper mapper)
        {
            _studentService = studentService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all the students in the database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StudentDTO>>> GetStudents() => Ok(_mapper.Map<List<StudentDTO>>(await _studentService.GetAllAsync()));
        

        [HttpGet("{id}/subjects")] // api/v1/students/5/subjects
        public async Task<ActionResult<IEnumerable<Subject>>> GetSubjectsForStudent(int id)
        {
            return Ok(await _studentService.GetSubjects(id));
        }

        [HttpPut("{id}/subjects")]
        public async Task<ActionResult> UpdateSubjectsForStudent(int id, int[] subjects)
        {
            await _studentService.UpdateSubjectsForStudent(id, subjects);
            return NoContent();
        }

        // GET: api/Student/5
        [HttpGet("{id}")]
        public async Task<ActionResult<StudentDTO>> GetStudent(int id)
        {
            var student = await _studentService.GetByIdAsync(id);

            if (student == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<StudentDTO>(student));
        }

        // PUT: api/Student/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStudent(int id, Student student)
        {
            if (id != student.Id)
            {
                return BadRequest();
            }

            await _studentService.UpdateAsync(student);

            return NoContent();
        }

        // POST: api/Student
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Student>> PostStudent(StudentPostDTO studentDTO)
        {
            Student student = new Student()
            {
                Name = studentDTO.Name,
                ProfessorId = studentDTO.Professor
            };
            await _studentService.AddAsync(student);

            return CreatedAtAction("GetStudent", new { id = student.Id }, student);
        }
    }
}
