using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;
using WebAPI_Ef_OpenAPI.Models;
using WebAPI_Ef_OpenAPI.Services.Students;

namespace WebAPI_Ef_OpenAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {

            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Postgrad API",
                    Description = "Simple API to manage postgraduate studies",
                    Contact = new OpenApiContact
                    {
                        Name = "Nicholas Lennox",
                        Url = new Uri("https://gitlab.com/NicholasLennox")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "MIT 2023",
                        Url = new Uri("https://opensource.org/licenses/MIT")
                    }
                });
                options.IncludeXmlComments(xmlPath);
            }
);
            builder.Services.AddDbContext<PostgradDbContext>(
                opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("default")));
            builder.Services.AddScoped<IStudentService, StudentService>();
            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}