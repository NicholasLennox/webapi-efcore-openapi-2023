﻿using WebAPI_Ef_OpenAPI.Models.Entities;

namespace WebAPI_Ef_OpenAPI.Services.Students
{
    public interface IStudentService: ICrudService<Student, int>
    {
        Task<IEnumerable<Subject>> GetSubjects(int id);
        Task UpdateSubjectsForStudent(int id, int[] subjects);
    }
}
