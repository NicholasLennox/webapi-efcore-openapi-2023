﻿using Microsoft.EntityFrameworkCore;
using WebAPI_Ef_OpenAPI.Models;
using WebAPI_Ef_OpenAPI.Models.Entities;

namespace WebAPI_Ef_OpenAPI.Services.Students
{
    public class StudentService : IStudentService
    {
        private readonly PostgradDbContext _context;

        public StudentService(PostgradDbContext context)
        {
            _context = context;
        }

        public async Task<Student> AddAsync(Student obj)
        {
            await _context.Students.AddAsync(obj);
            await _context.SaveChangesAsync();
            return obj;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Student>> GetAllAsync()
        {
            return await _context.Students.Include(s => s.Project).Include(s => s.Subjects).ToListAsync();
        }

        public async Task<Student> GetByIdAsync(int id)
        {
            return await _context.Students.FindAsync(id);
        }

        public async Task<IEnumerable<Subject>> GetSubjects(int id)
        {
            return (await _context.Students
                .Where(s => s.Id == id)
                .Include(s => s.Subjects)
                .FirstAsync()).Subjects;
        }

        public async Task<Student> UpdateAsync(Student obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return obj;
        }

        public async Task UpdateSubjectsForStudent(int id, int[] subjects)
        {
            var student = await _context.Students.Where(s => s.Id == id).Include(s => s.Subjects).FirstOrDefaultAsync();
            var subjectsEntity = new List<Subject>();
            foreach(var subject in subjects)
            {
                subjectsEntity.Add(await _context.Subjects.FindAsync(subject));
            }
            student.Subjects = subjectsEntity;
            _context.Entry(student).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
